let anchors = document.querySelectorAll('a[href*="#"]');

anchors.forEach(anchor => {
  anchor.addEventListener('click', function (event) {
    event.preventDefault();
    let anchorId = this.getAttribute('href');
    document.querySelector(anchorId).scrollIntoView({
      behavior: 'smooth',
    });
  });
});
